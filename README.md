# CloudRecipes #

A Set of now obsolete Linux Recipe's that contain steps to configure various  
servers / tools on CarIQ Linux VMs. This has no more use-case.

Please see [AnsiblePlaybooks](https://bitbucket.org/cariq_devops/ansibleplaybooks) repository  
for current automated CarIQ Servers.

### Author and Admins

* Vinu Kanakasabhapathy
* Joy Bhattacherjee
  * +91-9011235028
  * +91-8951285502